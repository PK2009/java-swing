/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plane;

import code.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author prathmesh
 */
public class air1 extends javax.swing.JPanel {

    /**
     * Creates new form air1
     */
      
     private Air jhk;
     private ArrayList<String> re;
            

     private ArrayList<ArrayList<String>> hg;
    public air1(ArrayList<String> re, Air jhk) {
        initComponents();
        this.jhk=jhk;
        this.re=re;
      hg = new ArrayList<ArrayList<String>>();
      populateTable();
      
    }
    
    public void populateTable()
  {
        
       DateTimeFormatter sdf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        LocalDate date2 = LocalDate.now();
     
      DefaultTableModel dtm = (DefaultTableModel)VVtbl.getModel();
      dtm.setRowCount(0);
      hg.add(re);
      
      
      for(ArrayList<String> re : hg)
      {
         if(re.size()==17)
          {
          Object row[] = new Object[9];
         row[0] = re.get(0);
         row[1] = re.get(1);
         row[2] = re.get(2);
         row[3] = re.get(3);
         row[4] = re.get(4);
         row[5] = re.get(5);
         row[6] = re.get(6);
         row[7] = re.get(7);
         row[8] = date2;

          dtm.addRow(row);
          Object row1[] = new Object[9];
          row1[0] = re.get(9);
         row1[1] = re.get(10);
         row1[2] = re.get(11);
         row1[3] = re.get(12);
         row1[4] = re.get(13);
         row1[5] = re.get(14);
         row1[6] = re.get(15);
         row1[7] = re.get(16);
         row1[8] = date2;
         dtm.addRow(row1);
        
          }
      
   
         if(re.size()==8)
          {
          Object row[] = new Object[9];
         row[0] = re.get(0);
         row[1] = re.get(1);
         row[2] = re.get(2);
         row[3] = re.get(3);
         row[4] = re.get(4);
         row[5] = re.get(5);
         row[6] = re.get(6);
         row[7] = re.get(7);
         row[8] = date2;

          dtm.addRow(row);
          }
      
     
           if(re.size()==9)
          {
          Object row[] = new Object[9];
         row[0] = re.get(0);
         row[1] = re.get(1);
         row[2] = re.get(2);
         row[3] = re.get(3);
         row[4] = re.get(4);
         row[5] = re.get(5);
         row[6] = re.get(6);
         row[7] = re.get(7);
                  row[8] = date2;

          dtm.addRow(row);
          }
           
               if(re.size()==7)
          {
          Object row[] = new Object[8];
         row[0] = re.get(0);
         row[1] = re.get(1);
         row[2] = re.get(2);
         row[4] = re.get(3);
         row[6] = re.get(4);
         row[7] = re.get(5);
         row[8] = date2;

          dtm.addRow(row);
          }
               
                    if(re.size()==10)
          {
          Object row[] = new Object[9];
         row[0] = re.get(0);
         row[1] = re.get(1);
         row[2] = re.get(2);
         row[4] = re.get(3);
         row[6] = re.get(4);
         row[7] = re.get(5);
         row[8] = date2;

          dtm.addRow(row);
          }
          
      }
      
           
      
    
  }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        VVtbl = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();

        VVtbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Name of airplane", "Available dates", "Company", "Year of manufacturing", "Number of seats(available)", "Serial Number", "Model number", "Airports (available)", "Last Updated date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(VVtbl);

        jButton1.setText("Delete");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(452, 452, 452)
                        .addComponent(jButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1367, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1)
                .addGap(22, 22, 22)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(156, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
      
         DateTimeFormatter sdf1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        LocalDateTime date2 = LocalDateTime.now();
        re.removeAll(re);
         hg.removeAll(hg);
        DefaultTableModel dtm = (DefaultTableModel)VVtbl.getModel();
     
      int rowCount = dtm.getRowCount();
for (int i = rowCount - 1; i >= 0; i--) {
    dtm.removeRow(i);

}
rowCount=0;
        jhk.getAir().removeAll(jhk.getAir());
                JOptionPane.showMessageDialog(null, "The last updated catalog time is is"+ date2, "Warning!!!!", JOptionPane.WARNING_MESSAGE);
                
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    
   


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable VVtbl;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

    
}
