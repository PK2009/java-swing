/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author prathmesh
 */
public class AccountDirectory {
    private ArrayList<Account> acclist;

    public AccountDirectory() {
        
        this.acclist = new ArrayList<Account>();
    }
    
    
    
    

    public ArrayList<Account> getAcclist() {
        return acclist;
    }

    public void setAcclist(ArrayList<Account> acclist) {
        this.acclist = acclist;
    }
    
    public Account addacc ()
    {
        Account account = new Account();
        acclist.add(account);
        return account;
    }
    
    public void delete(Account account)
    {
        acclist.remove(account);
    }
    
    public Account search(String AccountNumber)
    {
        for(Account acc : acclist)
        {
            if(acc.getAccountNumber().equals(AccountNumber))
                return acc;
        }
        return null;
    }
    
}
