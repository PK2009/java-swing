/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.ArrayList;
/**
 *
 * @author prathmesh
 */
public class VitalSignHistory {
    
        private ArrayList<VitalSigns> vitalsignHistory;

        public VitalSignHistory()
        {
            vitalsignHistory = new ArrayList<VitalSigns>();
            
        }

    public ArrayList<VitalSigns> getVitalsignHistory() {
        return vitalsignHistory;
    }

    public void setVitalsignHistory(ArrayList<VitalSigns> vitalsignHistory) {
        this.vitalsignHistory = vitalsignHistory;
    }
    
    public VitalSigns addvitals()
    {
        VitalSigns vs = new VitalSigns();
        vitalsignHistory.add(vs);
        return vs;
        
    }
    
    public void delete(VitalSigns v)
    {
     vitalsignHistory.remove(v);
    }
}
