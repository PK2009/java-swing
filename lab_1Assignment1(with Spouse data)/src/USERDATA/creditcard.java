/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package USERDATA;

/**
 *
 * @author prathmesh
 */
public class creditcard {
    private String cardnumber;
    private String exp;
    private String type;
    private String cardname;
    private String bank;
    
    private String cardnumbersp;
    private String expsp;
    private String typesp;
    private String cardnamesp;
    private String banksp;

    public String getCardnumbersp() {
        return cardnumbersp;
    }

    public void setCardnumbersp(String cardnumbersp) {
        this.cardnumbersp = cardnumbersp;
    }

    public String getExpsp() {
        return expsp;
    }

    public void setExpsp(String expsp) {
        this.expsp = expsp;
    }

    public String getTypesp() {
        return typesp;
    }

    public void setTypesp(String typesp) {
        this.typesp = typesp;
    }

    public String getCardnamesp() {
        return cardnamesp;
    }

    public void setCardnamesp(String cardnamesp) {
        this.cardnamesp = cardnamesp;
    }

    public String getBanksp() {
        return banksp;
    }

    public void setBanksp(String banksp) {
        this.banksp = banksp;
    }

    public String getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCardname() {
        return cardname;
    }

    public void setCardname(String cardname) {
        this.cardname = cardname;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }
}
