/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package USERDATA;

/**
 *
 * @author prathmesh
 */

public class Person {
    private address add;
    private creditcard card;
    private financialacc finan;
    private licensedata ldata;
    
    private String name;
    private String age;
    private String nationality;
    private String bgroup;
    private String img;
    
    private String namesp;
    private String agesp;
    private String nationalitysp;
    private String bgroupsp;
    private String imgsp;
    
  public Person()
 {
     
     add=new address();
     card=new creditcard();
     finan=new financialacc();
     ldata=new licensedata();
 }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getBgroup() {
        return bgroup;
    }

    public void setBgroup(String bgroup) {
        this.bgroup = bgroup;
    }
    
    public creditcard getCard() {
        return card;
    }

    public void setCard(creditcard card) {
        this.card = card;
    }

    public financialacc getFinan() {
        return finan;
    }

    public void setFinan(financialacc finan) {
        this.finan = finan;
    }

    public licensedata getLdata() {
        return ldata;
    }

    public void setLdata(licensedata ldata) {
        this.ldata = ldata;
    }
     public address getAdd() {
        return add;
    }

    public void setAdd(address add) {
        this.add = add;
    }
    
        public String getNamesp() {
        return namesp;
    }

    public void setNamesp(String namesp) {
        this.namesp = namesp;
    }

    public String getAgesp() {
        return agesp;
    }

    public void setAgesp(String agesp) {
        this.agesp = agesp;
    }

    public String getNationalitysp() {
        return nationalitysp;
    }

    public void setNationalitysp(String nationalitysp) {
        this.nationalitysp = nationalitysp;
    }

    public String getBgroupsp() {
        return bgroupsp;
    }

    public void setBgroupsp(String bgroupsp) {
        this.bgroupsp = bgroupsp;
    }

    public String getImgsp() {
        return imgsp;
    }

    public void setImgsp(String imgsp) {
        this.imgsp = imgsp;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
    
}
