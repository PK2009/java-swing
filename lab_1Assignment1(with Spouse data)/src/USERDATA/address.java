/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package USERDATA;

/**
 *
 * @author prathmesh
 */
public class address {
    
    private String addline1;
    private String city;
    private String state;
    private String country;
    private String zip;
    
    private String addline1sp;
    private String citysp;
    private String statesp;
    private String countrysp;
    private String zipsp;

    public String getAddline1sp() {
        return addline1sp;
    }

    public void setAddline1sp(String addline1sp) {
        this.addline1sp = addline1sp;
    }

    public String getCitysp() {
        return citysp;
    }

    public void setCitysp(String citysp) {
        this.citysp = citysp;
    }

    public String getStatesp() {
        return statesp;
    }

    public void setStatesp(String statesp) {
        this.statesp = statesp;
    }

    public String getCountrysp() {
        return countrysp;
    }

    public void setCountrysp(String countrysp) {
        this.countrysp = countrysp;
    }

    public String getZipsp() {
        return zipsp;
    }

    public void setZipsp(String zipsp) {
        this.zipsp = zipsp;
    }

    public String getAddline1() {
        return addline1;
    }

    public void setAddline1(String addline1) {
        this.addline1 = addline1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
}

