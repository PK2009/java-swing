/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package USERDATA;

/**
 *
 * @author prathmesh
 */
public class financialacc {
    private String checknum;
    private String savingnum;
    private String checkdate;
    private String savedate;
    private String checkdebitamt;
    private String checkcreditamt;
    private String savedebitamt;
    private String savecreditamt;
    private String savestatus;
    private String checkstatus;
    
       private String checknumsp;
    private String savingnumsp;
    private String checkdatesp;
    private String savedatesp;
    private String checkdebitamtsp;
    private String checkcreditamtsp;
    private String savedebitamtsp;
    private String savecreditamtsp;
    private String savestatussp;
    private String checkstatussp;

    public String getChecknumsp() {
        return checknumsp;
    }

    public void setChecknumsp(String checknumsp) {
        this.checknumsp = checknumsp;
    }

    public String getSavingnumsp() {
        return savingnumsp;
    }

    public void setSavingnumsp(String savingnumsp) {
        this.savingnumsp = savingnumsp;
    }

    public String getCheckdatesp() {
        return checkdatesp;
    }

    public void setCheckdatesp(String checkdatesp) {
        this.checkdatesp = checkdatesp;
    }

    public String getSavedatesp() {
        return savedatesp;
    }

    public void setSavedatesp(String savedatesp) {
        this.savedatesp = savedatesp;
    }

    public String getCheckdebitamtsp() {
        return checkdebitamtsp;
    }

    public void setCheckdebitamtsp(String checkdebitamtsp) {
        this.checkdebitamtsp = checkdebitamtsp;
    }

    public String getCheckcreditamtsp() {
        return checkcreditamtsp;
    }

    public void setCheckcreditamtsp(String checkcreditamtsp) {
        this.checkcreditamtsp = checkcreditamtsp;
    }

    public String getSavedebitamtsp() {
        return savedebitamtsp;
    }

    public void setSavedebitamtsp(String savedebitamtsp) {
        this.savedebitamtsp = savedebitamtsp;
    }

    public String getSavecreditamtsp() {
        return savecreditamtsp;
    }

    public void setSavecreditamtsp(String savecreditamtsp) {
        this.savecreditamtsp = savecreditamtsp;
    }

    public String getSavestatussp() {
        return savestatussp;
    }

    public void setSavestatussp(String savestatussp) {
        this.savestatussp = savestatussp;
    }

    public String getCheckstatussp() {
        return checkstatussp;
    }

    public void setCheckstatussp(String checkstatussp) {
        this.checkstatussp = checkstatussp;
    }
   

    public String getSavestatus() {
        return savestatus;
    }

    public void setSavestatus(String savestatus) {
        this.savestatus = savestatus;
    }

    public String getCheckstatus() {
        return checkstatus;
    }

    public void setCheckstatus(String checkstatus) {
        this.checkstatus = checkstatus;
    }

    public String getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(String checkdate) {
        this.checkdate = checkdate;
    }

    public String getSavedate() {
        return savedate;
    }

    public void setSavedate(String savedate) {
        this.savedate = savedate;
    }

    public String getCheckdebitamt() {
        return checkdebitamt;
    }

    public void setCheckdebitamt(String checkdebitamt) {
        this.checkdebitamt = checkdebitamt;
    }

    public String getCheckcreditamt() {
        return checkcreditamt;
    }

    public void setCheckcreditamt(String checkcreditamt) {
        this.checkcreditamt = checkcreditamt;
    }

    public String getSavedebitamt() {
        return savedebitamt;
    }

    public void setSavedebitamt(String savedebitamt) {
        this.savedebitamt = savedebitamt;
    }

    public String getSavecreditamt() {
        return savecreditamt;
    }

    public void setSavecreditamt(String savecreditamt) {
        this.savecreditamt = savecreditamt;
    }

    public String getChecknum() {
        return checknum;
    }

    public void setChecknum(String checknum) {
        this.checknum = checknum;
    }

    public String getSavingnum() {
        return savingnum;
    }

    public void setSavingnum(String savingnum) {
        this.savingnum = savingnum;
    }

}