/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package USERDATA;

/**
 *
 * @author prathmesh
 */
public class licensedata {
    private String lnumber;
    private String lexp;
    private String placeofissue;
    private String Ltype;
    private String DOB;
    
     private String lnumbersp;
    private String lexpsp;
    private String placeofissuesp;
    private String Ltypesp;
    private String DOBsp;

    public String getLnumbersp() {
        return lnumbersp;
    }

    public void setLnumbersp(String lnumbersp) {
        this.lnumbersp = lnumbersp;
    }

    public String getLexpsp() {
        return lexpsp;
    }

    public void setLexpsp(String lexpsp) {
        this.lexpsp = lexpsp;
    }

    public String getPlaceofissuesp() {
        return placeofissuesp;
    }

    public void setPlaceofissuesp(String placeofissuesp) {
        this.placeofissuesp = placeofissuesp;
    }

    public String getLtypesp() {
        return Ltypesp;
    }

    public void setLtypesp(String Ltypesp) {
        this.Ltypesp = Ltypesp;
    }

    public String getDOBsp() {
        return DOBsp;
    }

    public void setDOBsp(String DOBsp) {
        this.DOBsp = DOBsp;
    }

    public String getLnumber() {
        return lnumber;
    }

    public void setLnumber(String lnumber) {
        this.lnumber = lnumber;
    }

    public String getLexp() {
        return lexp;
    }

    public void setLexp(String lexp) {
        this.lexp = lexp;
    }

    public String getPlaceofissue() {
        return placeofissue;
    }

    public void setPlaceofissue(String placeofissue) {
        this.placeofissue = placeofissue;
    }

    public String getLtype() {
        return Ltype;
    }

    public void setLtype(String Ltype) {
        this.Ltype = Ltype;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }
}
