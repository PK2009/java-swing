/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code;

/**
 *
 * @author prathmesh
 */
public class Air2 {
    private String Name;
   private String Com;
   private String date;
   private String yrmf;
   private String srno;
   private String mno;
   private String airp;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getYrmf() {
        return yrmf;
    }

    public void setYrmf(String yrmf) {
        this.yrmf = yrmf;
    }

    public String getSrno() {
        return srno;
    }

    public void setSrno(String srno) {
        this.srno = srno;
    }

    public String getMno() {
        return mno;
    }

    public void setMno(String mno) {
        this.mno = mno;
    }

    public String getAirp() {
        return airp;
    }

    public void setAirp(String airp) {
        this.airp = airp;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getCom() {
        return Com;
    }

    public void setCom(String Com) {
        this.Com = Com;
    }
    
    @Override
    public String toString()
    {
        return this.Name;
        
    }
   
   
}
