/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.InventoryManager;
import Business.Role.Role;
import Business.VaccineDirectory;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class Inventory extends Organization{

    //private VaccineDirectory vacDir;
    public Inventory() {
        super(Organization.Type.Inventory.getValue());
        //vacDir = new VaccineDirectory();
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new InventoryManager());
        return roles;
    }
     
   
    
    
}
