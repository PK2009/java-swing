/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author prathmesh
 */
public class Diseasecatalog {
    
    
     private ArrayList<Disease> Diseaselist;

    public Diseasecatalog() {
        Diseaselist = new ArrayList<>();
    }

    public ArrayList<Disease> getDiseaselist() {
        return Diseaselist;
    }

    public void setDiseaselist(ArrayList<Disease> Diseaselist) {
        this.Diseaselist = Diseaselist;
    }
    
    
}
