/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Admin;

import java.util.ArrayList;

/**
 *
 * @author prathmesh
 */
public class Useraccdir {
    
     private ArrayList<Useracc> acclist;

    public Useraccdir() {
        
        acclist = new ArrayList<Useracc>();
    }

    public ArrayList<Useracc> getAcclist() {
        return acclist;
    }

    public void setAcclist(ArrayList<Useracc> acclist) {
        this.acclist = acclist;
    }
    
    public Useracc newuseracc()
    {
        
        Useracc abc = new Useracc();
        acclist.add(abc);
        return abc;
        
    }
    
    public void delete(Useracc acc)
    {
        acclist.remove(acc);
    }
  
    
     public Useracc isValiduser(String userid, String pwd)
    {
           for(Useracc acc : acclist)
        {
                for(HASHPasses mn : acc.getHas())
                {

            if(acc.getUserid().equalsIgnoreCase(userid) && mn.getHashpasswords().equals(pwd))
            {
                return acc;
            }
                }
        }
        return null;
    }
    
    
}
