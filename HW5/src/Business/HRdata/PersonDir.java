/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HRdata;

import java.util.ArrayList;

/**
 *
 * @author prathmesh
 */
public class PersonDir {
    
      private ArrayList<Person> perlist;

    public PersonDir() {
        
        perlist = new ArrayList<Person>();
    }

    public ArrayList<Person> getPerlist() {
        return perlist;
    }

    public void setPerlist(ArrayList<Person> perlist) {
        this.perlist = perlist;
    }
    
    
    public void delete(Person per)
    {
        perlist.remove(per);
    }
    
    public Person newPerson()
    {
        Person xyz = new Person();
        perlist.add(xyz);
        return xyz;
    } 
    
  
       public Person searchbylastname(String un)
    {
        for(Person acc : perlist)
        {
            if(acc.getLastName().equals(un))
                return acc;
        }
        return null;
    }
    
   
}
