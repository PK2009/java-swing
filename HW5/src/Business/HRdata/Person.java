/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HRdata;

import Business.Admin.Useracc;

/**
 *
 * @author prathmesh
 */
public class Person {
    
    String FirstName;
    String LastName;
    String SSN;

    
    private Useracc usacc;
 
    public Person()
    {
         usacc = new Useracc();
    }

    public Useracc getUsacc() {
        return usacc;
    }

    public void setUsacc(Useracc usacc) {
        this.usacc = usacc;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getSSN() {
        return SSN;
    }

    public void setSSN(String SSN) {
        this.SSN = SSN;
    }

   
    
    @Override
    public String toString()
    {
        return getFirstName() + " " + getLastName();
}
    
    
            
    
}
