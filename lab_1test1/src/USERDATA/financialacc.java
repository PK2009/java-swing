/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package USERDATA;

/**
 *
 * @author prathmesh
 */
public class financialacc {
    private String checknum;
    private String savingnum;
    private String checkdate;
    private String savedate;
    private String checkdebitamt;
    private String checkcreditamt;
    private String savedebitamt;
    private String savecreditamt;
    private String savestatus;
    private String checkstatus;
   

    public String getSavestatus() {
        return savestatus;
    }

    public void setSavestatus(String savestatus) {
        this.savestatus = savestatus;
    }

    public String getCheckstatus() {
        return checkstatus;
    }

    public void setCheckstatus(String checkstatus) {
        this.checkstatus = checkstatus;
    }

    public String getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(String checkdate) {
        this.checkdate = checkdate;
    }

    public String getSavedate() {
        return savedate;
    }

    public void setSavedate(String savedate) {
        this.savedate = savedate;
    }

    public String getCheckdebitamt() {
        return checkdebitamt;
    }

    public void setCheckdebitamt(String checkdebitamt) {
        this.checkdebitamt = checkdebitamt;
    }

    public String getCheckcreditamt() {
        return checkcreditamt;
    }

    public void setCheckcreditamt(String checkcreditamt) {
        this.checkcreditamt = checkcreditamt;
    }

    public String getSavedebitamt() {
        return savedebitamt;
    }

    public void setSavedebitamt(String savedebitamt) {
        this.savedebitamt = savedebitamt;
    }

    public String getSavecreditamt() {
        return savecreditamt;
    }

    public void setSavecreditamt(String savecreditamt) {
        this.savecreditamt = savecreditamt;
    }

    public String getChecknum() {
        return checknum;
    }

    public void setChecknum(String checknum) {
        this.checknum = checknum;
    }

    public String getSavingnum() {
        return savingnum;
    }

    public void setSavingnum(String savingnum) {
        this.savingnum = savingnum;
    }

}