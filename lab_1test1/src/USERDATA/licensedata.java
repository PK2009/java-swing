/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package USERDATA;

/**
 *
 * @author prathmesh
 */
public class licensedata {
    private String lnumber;
    private String lexp;
    private String placeofissue;
    private String Ltype;
    private String DOB;

    public String getLnumber() {
        return lnumber;
    }

    public void setLnumber(String lnumber) {
        this.lnumber = lnumber;
    }

    public String getLexp() {
        return lexp;
    }

    public void setLexp(String lexp) {
        this.lexp = lexp;
    }

    public String getPlaceofissue() {
        return placeofissue;
    }

    public void setPlaceofissue(String placeofissue) {
        this.placeofissue = placeofissue;
    }

    public String getLtype() {
        return Ltype;
    }

    public void setLtype(String Ltype) {
        this.Ltype = Ltype;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }
}
