/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Admin;

import Business.HRdata.Person;
import java.util.ArrayList;

/**
 *
 * @author prathmesh
 */
public class Useracc {
    
    String Userid;
    String pass;
    String Acctype;
    String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    private ArrayList<HASHPasses> has;
    
    public Useracc()
    {
        has = new ArrayList<HASHPasses>();
    }

    public HASHPasses hashpassarr(String m)
    {
        
        HASHPasses abc = new HASHPasses();
        has.add(abc);
        return abc;
    }
    
    public ArrayList<HASHPasses> getHas() {
        return has;
    }

    public void setHas(ArrayList<HASHPasses> has) {
        this.has = has;
    }

    public String getUserid() {
        return Userid;
    }

    public void setUserid(String Userid) {
        this.Userid = Userid;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getAcctype() {
        return Acctype;
    }

    public void setAcctype(String Acctype) {
        this.Acctype = Acctype;
    }
    
    @Override
    public String toString()
    { 
        return getUserid() + ""; 
    }
    
}
