/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Admin.Useraccdir;
import Business.HRdata.PersonDir;

/**
 *
 * @author prathmesh
 */
public class Mainbusiness {
    
    private String name;
    private PersonDir persondirectory; 
    private Useraccdir useraccountsdirectory;
    
    public Mainbusiness(String n)
    {
        this.name = n;
        persondirectory = new PersonDir();
        useraccountsdirectory = new Useraccdir();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PersonDir getPersondirectory() {
        return persondirectory;
    }

    public void setPersondirectory(PersonDir persondirectory) {
        this.persondirectory = persondirectory;
    }

    public Useraccdir getUseraccountsdirectory() {
        return useraccountsdirectory;
    }

    public void setUseraccountsdirectory(Useraccdir useraccountsdirectory) {
        this.useraccountsdirectory = useraccountsdirectory;
    }
    
}
