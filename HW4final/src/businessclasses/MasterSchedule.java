/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessclasses;

/**
 *
 * @author kaushikp
 */
public class MasterSchedule {
    
    private AirLinerDirectory airLinerDir;
    
    public MasterSchedule(){
        airLinerDir = new AirLinerDirectory();
    }

    public AirLinerDirectory getAirLinerDir() {
        return airLinerDir;
    }

    public void setAirLinerDir(AirLinerDirectory airLinerDir) {
        this.airLinerDir = airLinerDir;
    }
    
}
