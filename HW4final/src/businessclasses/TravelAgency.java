/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessclasses;

/**
 *
 * @author prathmesh
 */
public class TravelAgency {
    private MasterSchedule ms;
    
     private CustomerDirectory custDir;
    
    public TravelAgency(){
        custDir = new CustomerDirectory();
         ms = new MasterSchedule();
    }

    public CustomerDirectory getCustDir() {
        return custDir;
    }

    public void setCustDir(CustomerDirectory custDir) {
        this.custDir = custDir;
    }
    
    

    public MasterSchedule getMs() {
        return ms;
    }

    public void setMs(MasterSchedule ms) {
        this.ms = ms;
    }
    
}
