/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessclasses;

import java.sql.Time;
import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author kaushikp
 */
public class Flight {
    
    private String name;
    private String src;
    private String dest;
    private Date date;
    private Time depTime;
    private int availableSeats;
   

    public int getAvailableSeats() {
        return availableSeats;
    }


    public void setAvailableSeats(int availableSeats) {
        this.availableSeats = availableSeats;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getDepTime() {
        return depTime;
    }

    public void setDepTime(Time depTime) {
        this.depTime = depTime;
    }
    
    @Override
    public String toString(){
        return name;
    }
    
}
