/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessclasses;

/**
 *
 * @author kaushikp
 */
public class Customer {
    private String firsname;
    private String lasname;
    private String custID;
    private String nat;

    public String getFname() {
        return firsname;
    }

    public void setFname(String firsname) {
        this.firsname = firsname;
    }

    public String getLname() {
        return lasname;
    }

    public void setLname(String lasname) {
        this.lasname = lasname;
    }

    public String getCustId() {
        return custID;
    }

    public void setCustId(String custID) {
        this.custID = custID;
    }

    public String getNationality() {
        return nat;
    }

    public void setNationality(String nat) {
        this.nat = nat;
    }
    
}
