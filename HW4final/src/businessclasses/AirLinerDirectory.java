/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessclasses;

import java.util.ArrayList;

/**
 *
 * @author kaushikp
 */
public class AirLinerDirectory {
    
    private ArrayList<AirLiner> airLinerDir;
    
    public AirLinerDirectory(){
        
        airLinerDir = new ArrayList<>();
    }

    public ArrayList<AirLiner> getAirLinerDir() {
        return airLinerDir;
    }

    public void setAirLinerDir(ArrayList<AirLiner> airLinerDir) {
        this.airLinerDir = airLinerDir;
    }
    
    public AirLiner addAirLine(){
        AirLiner a = new AirLiner();
        airLinerDir.add(a);
        return a;
    }    
    
}
