/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessclasses;

import java.util.ArrayList;

/**
 *
 * @author kaushikp
 */
public class FlightSchedule {
 
    private ArrayList<Flight> flightSched;
    
    public FlightSchedule(){
        flightSched = new ArrayList<>();
    }

    public ArrayList<Flight> getFlightSched() {
        return flightSched;
    }

    public void setFlightSched(ArrayList<Flight> flightSched) {
        this.flightSched = flightSched;
    }
    
    public Flight addFlight(){
        Flight f = new Flight();
        flightSched.add(f);
        return f;
    }
}
