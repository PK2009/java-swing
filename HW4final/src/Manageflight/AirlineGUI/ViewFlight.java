/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Manageflight.AirlineGUI;

import businessclasses.Flight;
import java.awt.CardLayout;
import java.awt.Component;
import java.sql.Date;
import java.sql.Time;
import javax.swing.JPanel;

/**
 *
 * @author kaushikp
 */
public class ViewFlight extends javax.swing.JPanel {

    /**
     * Creates new form ViewFlight
     */
    private JPanel cardSequenceJpanel;
    private Flight f;
    public ViewFlight(JPanel cardSequenceJPanel, Flight f) {
        initComponents();
        this.cardSequenceJpanel=cardSequenceJPanel;
        this.f = f;
        nameTxtField.setText(f.getName());
        srcTxtField.setText(f.getSrc());
        destTxtField.setText(f.getDest());
        dateTxttField.setText(f.getDate().toString());
        deptimeTxtField.setText(f.getDepTime().toString());
        seatsTxtField.setText(String.valueOf(f.getAvailableSeats()));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnBack = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        nameTxtField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        srcTxtField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        destTxtField = new javax.swing.JTextField();
        dateTxttField = new javax.swing.JTextField();
        deptimeTxtField = new javax.swing.JTextField();
        seatsTxtField = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        btnUpdate = new javax.swing.JButton();

        btnBack.setText("<--Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jLabel2.setText("Name:");

        nameTxtField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameTxtFieldActionPerformed(evt);
            }
        });

        jLabel3.setText("Source:");

        srcTxtField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                srcTxtFieldActionPerformed(evt);
            }
        });

        jLabel4.setText("Destination:");

        destTxtField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                destTxtFieldActionPerformed(evt);
            }
        });

        dateTxttField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dateTxttFieldActionPerformed(evt);
            }
        });

        deptimeTxtField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deptimeTxtFieldActionPerformed(evt);
            }
        });

        seatsTxtField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                seatsTxtFieldActionPerformed(evt);
            }
        });

        jLabel7.setText("Available Seats:");

        jLabel6.setText("Departure Time:");

        jLabel5.setText("Date:");

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        jLabel1.setText("Flight Details");

        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(227, 227, 227)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addGap(58, 58, 58)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(seatsTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(deptimeTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dateTxttField, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(destTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(srcTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nameTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnSave)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnUpdate))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(btnBack)
                        .addGap(209, 209, 209)
                        .addComponent(jLabel1)))
                .addContainerGap(239, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(btnBack)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 75, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(nameTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(srcTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(destTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(dateTxttField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(deptimeTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(seatsTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave)
                    .addComponent(btnUpdate))
                .addGap(151, 151, 151))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        cardSequenceJpanel.remove(this);
        Component[] componentArray = cardSequenceJpanel.getComponents();
        Component component = componentArray[componentArray.length -1];
        ManageFlights mf = (ManageFlights) component;
        mf.populateTbl();
        CardLayout layout = (CardLayout) cardSequenceJpanel.getLayout();
        layout.previous(cardSequenceJpanel);
    }//GEN-LAST:event_btnBackActionPerformed

    private void nameTxtFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameTxtFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nameTxtFieldActionPerformed

    private void srcTxtFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_srcTxtFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_srcTxtFieldActionPerformed

    private void destTxtFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_destTxtFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_destTxtFieldActionPerformed

    private void dateTxttFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dateTxttFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dateTxttFieldActionPerformed

    private void deptimeTxtFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deptimeTxtFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deptimeTxtFieldActionPerformed

    private void seatsTxtFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_seatsTxtFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_seatsTxtFieldActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
        
        f.setName(nameTxtField.getText());
        f.setSrc(srcTxtField.getText());
        f.setDest(destTxtField.getText());
        f.setDate(Date.valueOf(dateTxttField.getText()));
        f.setDepTime(Time.valueOf(deptimeTxtField.getText()));
        f.setAvailableSeats(Integer.valueOf(seatsTxtField.getText()));
        
        nameTxtField.setEditable(false);
        srcTxtField.setEditable(false);
        destTxtField.setEditable(false);
        dateTxttField.setEditable(false);
        deptimeTxtField.setEditable(false);
        seatsTxtField.setEditable(false);
        btnSave.setEnabled(false);
        btnUpdate.setEnabled(true);
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        // TODO add your handling code here:
        nameTxtField.setEditable(true);
        srcTxtField.setEditable(true);
        destTxtField.setEditable(true);
        dateTxttField.setEditable(true);
        deptimeTxtField.setEditable(true);
        seatsTxtField.setEditable(true);
        btnSave.setEnabled(true);
        btnUpdate.setEnabled(false);
    }//GEN-LAST:event_btnUpdateActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JTextField dateTxttField;
    private javax.swing.JTextField deptimeTxtField;
    private javax.swing.JTextField destTxtField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JTextField nameTxtField;
    private javax.swing.JTextField seatsTxtField;
    private javax.swing.JTextField srcTxtField;
    // End of variables declaration//GEN-END:variables
}
