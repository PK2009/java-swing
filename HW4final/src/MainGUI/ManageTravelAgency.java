/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainGUI;

import ManageairGUI.ManageAirLiner;
import ManageCustGUI.CustomerSearch;
import SearchflightGUI.SearchFlight;
import businessclasses.*;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author prathmesh
 */
public class ManageTravelAgency extends javax.swing.JPanel {

    /**
     * Creates new form ManageTravelAgency
     */
    private JPanel cardsequenceJPanel;
    private TravelAgency ta;
    private CustomerDirectory ty;
    public ManageTravelAgency(JPanel cardSequenceJPanel) {
        initComponents();
        this.cardsequenceJPanel = cardSequenceJPanel;
        ta = new TravelAgency();
           ty= ta.getCustDir();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnManageAirLiner = new javax.swing.JButton();
        btnSearchFlights = new javax.swing.JButton();
        btnManageCust = new javax.swing.JButton();

        btnManageAirLiner.setText("Manage AirLiner");
        btnManageAirLiner.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManageAirLinerActionPerformed(evt);
            }
        });

        btnSearchFlights.setText("Search Flights");
        btnSearchFlights.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchFlightsActionPerformed(evt);
            }
        });

        btnManageCust.setText("Manage Customers");
        btnManageCust.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManageCustActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(210, 210, 210)
                        .addComponent(btnManageCust))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(52, 52, 52)
                        .addComponent(btnManageAirLiner, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(61, 61, 61)
                        .addComponent(btnSearchFlights, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(228, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(137, 137, 137)
                .addComponent(btnManageAirLiner)
                .addGap(18, 18, 18)
                .addComponent(btnManageCust)
                .addGap(53, 53, 53)
                .addComponent(btnSearchFlights)
                .addContainerGap(159, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnManageCustActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManageCustActionPerformed
        // TODO add your handling code here:
        CustomerSearch cs = new CustomerSearch(cardsequenceJPanel,ta.getMs().getAirLinerDir(), ty );
        cardsequenceJPanel.add("ManageAirLiner", cs);
        CardLayout layout = (CardLayout) cardsequenceJPanel.getLayout();
        layout.next(cardsequenceJPanel);
    }//GEN-LAST:event_btnManageCustActionPerformed

    private void btnManageAirLinerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManageAirLinerActionPerformed
        // TODO add your handling code here:
        ManageAirLiner ma = new ManageAirLiner(cardsequenceJPanel,ta.getMs().getAirLinerDir() );
        cardsequenceJPanel.add("ManageAirLiner", ma);
        CardLayout layout = (CardLayout) cardsequenceJPanel.getLayout();
        layout.next(cardsequenceJPanel);
    }//GEN-LAST:event_btnManageAirLinerActionPerformed

    private void btnSearchFlightsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchFlightsActionPerformed
        // TODO add your handling code here:
        SearchFlight sf = new SearchFlight(cardsequenceJPanel,ta.getMs().getAirLinerDir());
        cardsequenceJPanel.add("SearchFlight", sf);
        CardLayout layout = (CardLayout) cardsequenceJPanel.getLayout();
        layout.next(cardsequenceJPanel);
    }//GEN-LAST:event_btnSearchFlightsActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnManageAirLiner;
    private javax.swing.JButton btnManageCust;
    private javax.swing.JButton btnSearchFlights;
    // End of variables declaration//GEN-END:variables
}
