package Business;

import Business.Employee.Employee;
import Business.Role.SystemAdminRole;
import Business.UserAccount.UserAccount;

/**
 *
 * @author rrheg
 */
public class ConfigureASystem {
    
    public static EcoSystem configure(){
        
        EcoSystem system = EcoSystem.getInstance();
        
        //Create a network
        //create an enterprise
        //initialize some organizations
        //have some employees 
        //create user account
        
        
        Employee employee = system.getEmployeeDirectory().createEmployee("RRH");
        
        UserAccount ua = system.getUserAccountDirectory().createUserAccount("sysadmin", "sysadmin", employee, new SystemAdminRole());
        
        
        VaccineDirectory vacdir = system.getVacdir();
        Vaccine v = vacdir.createVaccine();
        v.setName("alpha");
        v.setAvailrequest(10);
        Disease d = v.getDisease();
        d.setDisname("AIDS");
        v.setDisease(d);
        
        
        
        
        return system;
    }
    
}
