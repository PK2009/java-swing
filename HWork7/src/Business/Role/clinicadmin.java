/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.EnterpriseDirectory;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;
import userinterface.SystemAdminWorkArea.SystemAdminWorkAreaJPanel;
import userinterface.clinicadmin.clinicadminworkJPanel;

/**
 *
 * @author kaushikp
 */
public class clinicadmin extends Role {
    
    
    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, EnterpriseDirectory enterpriseDirectory, Enterprise enterprise, EcoSystem system) {
        return new clinicadminworkJPanel(userProcessContainer, organization);
    }
    
    
}
