/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.EnterpriseDirectory;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import userinterface.Inventorymanagement.Inventoryworkarea;
import javax.swing.JPanel;

/**
 *
 * @author raunak
 */
public class InventoryManager extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, EnterpriseDirectory enterpriseDirectory, Enterprise enterprise, EcoSystem business) {
        return new Inventoryworkarea(userProcessContainer,account,organization,enterprise,enterpriseDirectory);
    }
    
}
