/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author prathmesh
 */
public class Vaccine {
    
   private String name;
   private Disease disease;
   private int availrequest;

    public Vaccine() {
        disease = new Disease();
    }

    public Disease getDisease() {
        return disease;
    }

    public void setDisease(Disease disease) {
        this.disease = disease;
    }
   
   
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAvailrequest() {
        return availrequest;
    }

    public void setAvailrequest(int availrequest) {
        this.availrequest = availrequest;
    }
    
    @Override
    public String toString()
    {
        return name;
    }
    
}
