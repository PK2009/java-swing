/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Employee.Employee;
import java.util.ArrayList;

/**
 *
 * @author prathmesh
 */
public class VaccineDirectory {
    private ArrayList<Vaccine> vaccineList;

    public VaccineDirectory() {
        vaccineList = new ArrayList<>();
    }
    

    public ArrayList<Vaccine> getVaccineList() {
        return vaccineList;
    }

    public void setVaccineList(ArrayList<Vaccine> vaccineList) {
        this.vaccineList = vaccineList;
    }
    
    public Vaccine createVaccine(){
        Vaccine vacc = new Vaccine();
        vaccineList.add(vacc);
        return vacc;
    }
    
    
}
