/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author kaushikp
 */
public class SatelliteClinic extends Enterprise{

    public SatelliteClinic(String name, EnterpriseType type) {
        super(name, EnterpriseType.SatelliteClinic);
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        return null;
    }
    
}
