/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Enterprise.Enterprise.EnterpriseType;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class EnterpriseDirectory {
    
    private ArrayList<Enterprise> enterpriseList;

    public EnterpriseDirectory() {
        enterpriseList = new ArrayList<>();
    }

    public ArrayList<Enterprise> getEnterpriseList() {
        return enterpriseList;
    }
    
        public Enterprise createAndAddEnterprise(String name, Enterprise.EnterpriseType type){
        Enterprise enterprise = null;
        if (type == Enterprise.EnterpriseType.Publichealth){
            enterprise = new Publichealth(name, EnterpriseType.Publichealth);
            enterpriseList.add(enterprise);
        }
        else if (type == Enterprise.EnterpriseType.CDC){
            enterprise = new CDC(name, EnterpriseType.CDC);
            enterpriseList.add(enterprise);
        }
        else if (type == Enterprise.EnterpriseType.Distributors){
            enterprise = new Distributors(name, EnterpriseType.Distributors);
            enterpriseList.add(enterprise);
        }
        else if (type == Enterprise.EnterpriseType.SatelliteClinic){
            enterprise = new SatelliteClinic(name, EnterpriseType.SatelliteClinic);
            enterpriseList.add(enterprise);
        }
        
        return enterprise;
    }
    
}
